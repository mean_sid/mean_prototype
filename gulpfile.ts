'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let autoprefixer = require('gulp-autoprefixer');



gulp.task('sass', function () {
    return gulp.src('src/client/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('src/client/'));
});

gulp.task('sass-components', function () {
    return gulp.src('src/client/app/**/*.component.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 5 version', 'ie 9'))
        .pipe(gulp.dest('src/client/app/'));
});


// Default
gulp.task('default', ['watch']);



////////////////////////////////////////



const del = require('del');
const tsc = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const tsProjectClient = tsc.createProject('src/client/tsconfig.json');
const tsProjectServer = tsc.createProject('src/server/tsconfig.json');
const tslint = require('gulp-tslint');
const server = require('gulp-develop-server');
const sequence = require('run-sequence');
const wait = require('gulp-wait');
const exec = require('child_process').exec;
const livereload = require('gulp-livereload');

// run server
const serverOptions = { path: './dist/server/index.js' };

gulp.task( 'server:start', function() {
    server.listen(serverOptions);
    livereload({start: true, quiet: true});
});

// restart server if app.js changed
gulp.task( 'server:restart', function() {
    setTimeout(function () {
        server.restart();
    }, 5000); // time needed to compile server ts files before restart server
});

/**
 * Remove build directory.
 */
gulp.task('clean', (cb) => {
    return del(['dev-dist/client'], cb);
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', () => {
    return gulp.src(['src/**/*.ts'])
        .pipe(tslint({
            formatter: 'prose'
        }))
        .pipe(tslint.report());
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task('compile_client', [], () => {
    const tsResult = gulp.src('src/client/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProjectClient());
    return tsResult.js
        .pipe(sourcemaps.write('.', {sourceRoot: '/src/client'}))
        .pipe(gulp.dest('dev-dist/client'))
        .pipe(livereload());
});

gulp.task('compile_server', [], () => {
    const tsResult = gulp.src('src/server/**/*.ts')
        .pipe(tsProjectServer());
    return tsResult.js
        .pipe(gulp.dest('dist/server'));
});

gulp.task('compile_server_packages', () => {
    exec('npm run compile-server-packages');
});


/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task('resources', () => {
    return gulp.src(['src/client/**/*', '!**/*.ts', '!**/*.scss', '!scss'])
        .pipe(gulp.dest('dev-dist/client'))
        .pipe(livereload());
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', ['server:start'], function () {
    gulp.watch(['src/client/**/*.ts']).on('change', function () {
        sequence('clean', 'compile_client', 'resources');
    });

    gulp.watch(['src/client/**/*.html', 'src/client/**/*.css', 'src/client/**/*.js'], ['resources']);

    gulp.watch(['src/server/**/*.ts'], ['compile_server', 'server:restart']);

    gulp.watch('src/client/scss/**/*.scss', ['sass', 'resources']);

    gulp.watch('src/client/app/**/*.component.scss', ['sass-components', 'resources']);
});

/**
 * Build the project.
 */
gulp.task('build', sequence('clean', 'compile_client', 'resources', 'compile_server'));
