import {Component, OnInit} from '@angular/core';
import {SortablejsOptions} from 'angular-sortablejs';
import {SortableService} from '../sortable.service';

/**
 * Основной компонент демонстрационного модуля Sortable.
 */
@Component({
    selector: 'app-sortable',
    templateUrl: './sortable.component.html',
    styleUrls: ['./sortable.component.css'],
})
export class SortableComponent implements OnInit {
    /**
     * Массив элементов в левой колонке. Связан с шаблоном.
     */
    items1: Array<string>;

    /**
     * Массив элементов в правой колонке. Связан с шаблоном.
     */
    items2: Array<string>;

    /**
     * Объект настроек для работы подключаемого модуля Sortablejs.
     *
     * @type {{}}
     */
    options: SortablejsOptions = {};

    /**
     * Inject зависимостей.
     * конфигурирование настроек подключаемого модуля Sortablejs.
     *
     * @param sortableService
     */
    constructor(private sortableService: SortableService) {
        this.options = {
            onUpdate: (event: any) => {
                console.log('update');
            },
            group: 'test',
        };
    }

    /**
     * Запрос на сервер и сохранение данных о элементах в обоих колонках.
     */
    ngOnInit() {
        this.sortableService.getItems(1).then((items) => this.items1 = items);
        this.sortableService.getItems(2).then((items) => this.items2 = items);
    }
}
