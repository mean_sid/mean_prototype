import {async, ComponentFixture, TestBed, tick} from '@angular/core/testing';
import {SortableComponent} from './sortable.component';
import {SortablejsModule} from 'angular-sortablejs';
import {SortableService} from '../sortable.service';
import {By} from '@angular/platform-browser';


class MockSortableService {
    getItems(group: number): Promise<any> {
        return new Promise((resolve, reject) => {
            switch (group) {
                case 1:
                    resolve([
                        {
                            id: (Math.floor(Math.random() * 100)),
                            name: 'test device 1'
                        },
                        {
                            id: (Math.floor(Math.random() * 100)),
                            name: 'test device 2'
                        },
                    ]);
                    break;
                case 2:
                    resolve([
                        {
                            id: (Math.floor(Math.random() * 100)),
                            name: 'test device 3'
                        },
                        {
                            id: (Math.floor(Math.random() * 100)),
                            name: 'test device 4'
                        },
                    ]);
                    break;
            }
        });
    }
}

describe('SortableComponent', () => {
    let component: SortableComponent;
    let fixture: ComponentFixture<SortableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SortableComponent],
            imports: [SortablejsModule],
            providers: [{provide: SortableService, useClass: MockSortableService}]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SortableComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('must have sortable elements', () => {
        fixture.whenStable().then(() => {
            fixture.detectChanges();

            expect(component.items1).toBeTruthy();
            expect(component.items2).toBeTruthy();

            let items1 = fixture.debugElement.queryAll(By.css('.items1 div'));
            expect(items1.length).toEqual(2);

            let items2TestElement = fixture.debugElement.query(By.css('.items2 div:first-child')).nativeElement;
            expect(items2TestElement.innerText).toEqual('test device 3');
        });
    });
});
