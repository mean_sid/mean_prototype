import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

/**
 * Сервис, содержащий логику работы с сервером, необходимую для модуля Sortable.
 */
@Injectable()
export class SortableService {
    /**
     * Адрес для запроса на сервер.
     *
     * @type {string}
     */
    private url = '/api/sortable/items/';

    /**
     * Inject зависимостей.
     *
     * @param http
     */
    constructor (private http: Http) {}

    /**
     * Метод запрашивает элементы с сервера по id.
     *
     * @param group
     * @returns {Promise<any>}
     */
    getItems(group: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.url + group).subscribe((res) => {
                let elements = JSON.parse(res.text());
                resolve(elements);
            });
        });
    }
}
