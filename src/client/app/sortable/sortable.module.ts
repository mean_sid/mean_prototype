///<reference path="../../../../node_modules/angular-sortablejs/dist/src/sortablejs.module.d.ts"/>
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SortablejsModule} from 'angular-sortablejs';
import {SortableComponent} from './sortable/sortable.component';
import {SortableService} from './sortable.service';


@NgModule({
    imports: [
        CommonModule,
        SortablejsModule
    ],
    declarations: [
        SortableComponent,
    ],
    exports: [SortableComponent],
    providers: [SortableService]
})
export class SortableModule {

}
