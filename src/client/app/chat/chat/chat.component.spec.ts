import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatComponent} from './chat.component';
import {ChatService} from '../chat.service';
import {SocketService} from '../../socket/socket.service';

describe('ChatComponent', () => {
    let component: ChatComponent;
    let fixture: ComponentFixture<ChatComponent>;
    let socketService: SocketService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ChatComponent],
            providers: [ChatService, SocketService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatComponent);
        component = fixture.componentInstance;
        socketService = fixture.debugElement.injector.get(SocketService);
        spyOn(socketService, 'create').and.callFake(() => {
            return new WebSocket('wss://echo.websocket.org');
        });
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('test socket connection created', () => {
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(component.socket).toBeTruthy();
        });
    });

    it('save message', async(done) => {
        component.socket.onopen = () => {
            component.socket.onmessage = (message) => {
                expect(message.data).toEqual('test message');
                done();
            };
            component.saveMessage('test message');
        };
    });

    it('show message', async(done) => {
        component.socket.onopen = () => {
            let testMessages = JSON.stringify(['test message 1', 'test message 2']);
            component.showMessages(testMessages);
            fixture.detectChanges();
            expect(component.messages).toEqual(['test message 1', 'test message 2']);
        };
    });
});
