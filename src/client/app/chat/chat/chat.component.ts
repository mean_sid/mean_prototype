import {Component, OnInit, } from '@angular/core';
import {ChatService} from '../chat.service';
import {SocketService} from '../../socket/socket.service';

/**
 * Основной компонент чата.
 */
@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
    /**
     * Свойство хранит WebSocket объект соединения с сервером.
     */
    public socket: WebSocket;

    /**
     * Массив сообщений, связаный с шаблоном.
     * Непосредственно отображается пользователю.
     */
    public messages: Array<string>;

    /**
     * Inject зависимостей
     *
     * @param chatService
     * @param socketService
     */
    constructor(
        private chatService: ChatService,
        private socketService: SocketService
    ) {}

    /**
     * Установка и настройка WebSocket соединения.
     */
    ngOnInit() {
        this.socket = this.socketService.create('node', 'chat');

        this.socket.onmessage = (message: any) => {
            this.showMessages(message);
        };
    }

    /**
     * Метод отправки сообщения через WebSocket
     *
     * @param message
     */
    saveMessage (message: string) {
        this.socket.send(message);
    }

    /**
     * Метод, обновляющий отображаемые пользователю сообщения.
     *
     * @param data
     */
    showMessages (data: any) {
        this.messages = JSON.parse(data.data);
    }
}
