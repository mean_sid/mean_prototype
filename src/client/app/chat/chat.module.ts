import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ChatComponent} from './chat/chat.component';
import {ChatService} from './chat.service';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        ChatComponent
    ],
    exports: [
        ChatComponent
    ],
    providers: [
        ChatService
    ]
})
export class ChatModule {
}
