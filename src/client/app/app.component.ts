import {Component} from '@angular/core';

/**
 * Корневой компонент приложения.
 */
@Component({
  selector: 'app-registration',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {}
