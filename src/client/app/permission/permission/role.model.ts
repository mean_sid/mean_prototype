import {Permission} from './permission.model';

/**
 * Модель, определяющая свойства для объекта Role.
 */
export class Role {
    /**
     * Название роли.
     */
    name: string;

    /**
     * Массив объектов доступа, определенных для данной роли.
     */
    permissions: Permission[];
}
