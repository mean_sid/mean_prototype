import {Component, OnInit} from '@angular/core';
import {PermissionService} from '../permission.service';
import {User} from './user.model';
import {Role} from './role.model';
import {Resource} from './resource.model';

/**
 * Корневой и основной компонент системы управления
 * пользователями, ролями и доступами.
 */
@Component({
    selector: 'app-permission',
    templateUrl: './permission.component.html',
    styleUrls: ['./permission.component.css']
})
export class PermissionComponent {
    /**
     * Массив пользователей. Привязан к шаблону.
     */
    public users: User[];

    /**
     * Массив ролей (с настройками доступов). Привязан к шаблону.
     */
    public roles: Role[];

    /**
     * Массив ресурсов, доступных к настройке доступов. Привязан к шаблону.
     */
    public resources: Resource[];

    /**
     * Объект пользователя, свойства которого привязаны к полям формы шаблона.
     * @type {User}
     */
    public newUser = new User;

    /**
     * Объект роли, свойства которой привязаны к полям формы шаблона.
     * @type {Role}
     */
    public newRole = new Role;

    /**
     * Свойство для хранения объекта роли, которая в данный момент выбрана
     * для редактирования доступов.
     */
    public selectedRole: Role;

    /**
     * Свойство принимает и отображает в шаблоне сообщения об ошибках
     * при создании пользователя.
     */
    public createUserErrorMessage: string;

    /**
     * Свойство принимает и отображает в шаблоне сообщения об ошибках
     * при создании роли.
     */
    public createRoleErrorMessage: string;

    /**
     * Inject зависимостей.
     * Запрос всех, необходимых для работы данных, и их обработка.
     */
    constructor (private permissionService: PermissionService) {
        this.setupUsers().then(() => {
            this.setupRoles().then(() => {
                this.setupResources().then();
            });
        });
    }

    /**
     *  Метод обрабатывает объект ответа сервера и создает\пересоздает
     *  массив пользователей
     */
    private setUsers(usersObj: any) {
        this.users = [];
        for (let key in usersObj) {
            if (usersObj.hasOwnProperty(key)) {
                this.users.push(usersObj[key]);
            }
        }
    }

    /**
     *  Метод обрабатывает объект ответа сервера и создает\пересоздает
     *  массив ролей.
     */
    private setRolesPermissions(rolesObj: any) {
        this.roles = [];
        for (let key in rolesObj) {
            if (rolesObj.hasOwnProperty(key)) {
                this.roles.push({name: key, permissions: rolesObj[key]});
            }
        }
    }

    /**
     * Метод получает пользователей, обрабатывает их и сохраняет в компоненте.
     * Возвращает boolean - успешно ли отработал метод.
     *
     * @returns {Promise<boolean>}
     */
    private setupUsers(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.permissionService.getUsers().then((users) => {
                this.setUsers(users);
                resolve(true);
            }).catch(() => {
                reject(false);
            });
        });
    }

    /**
     * Метод получает роли, обрабатывает и сохраняет в компоненте.
     * Возвращает boolean - успешно ли отработал метод.
     *
     * @returns {Promise<boolean>}
     */
    private setupRoles(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.permissionService.getRolesPermissions().then((roles) => {
                this.setRolesPermissions(roles);
                resolve(true);
            }).catch(() => {
                reject(false);
            });
        });
    }

    /**
     * Метод получает ресурсы, обрабатывает и сохраняет в компоненте.
     * Возвращает boolean - успешно ли отработал метод.
     *
     * @returns {Promise<boolean>}
     */
    private setupResources(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.permissionService.getResources().then((resources: Resource[]) => {
                this.resources = resources;
                resolve(true);
            });
        });
    }

    /**
     * Метод создания пользователя. Вызывается при отправке формы создания пользователя.
     * Использует this.newUser объект в качестве данных для создания пользователя.
     */
    public createUser() {
        if (this.newUser.name && this.newUser.password && this.newUser.role) {
            console.log(this.newUser);
            this.permissionService.createUser(this.newUser).then((result) => {
                console.log(result);
                if (result) {
                    this.setupUsers().then();
                    this.newUser = new User;
                    this.createUserErrorMessage = null;
                }
            }).catch((err) => {
                this.createUserErrorMessage = err;
            });
        } else {
            this.createUserErrorMessage = 'Name and Password fields are required';
        }
    }

    /**
     * Метод удаления пользователя. Вызывается в шаблоне, при попытке удалить пользователя.
     * Шаблон передает в метод строку-имя удаляемого пользователя.
     * Если результат удаления успешен, метод вызывает обновление данных о пользователях в компоненте.
     *
     * @param name
     */
    public deleteUser(name: string) {
        this.permissionService.deleteUser(name).then((result) => {
            if (result) {
                this.setupUsers().then();
            }
        });
    }

    /**
     * Метод изменяет роль пользователя. Вызывается в шаблоне, при попытке изменить роль пользователя.
     * Шаблон передает в метод имя редактируемого пользователя и новую роль.
     * Если результат удаления успешен, метод вызывает обновление данных о пользователях в компоненте.
     *
     * @param userName
     * @param newRole
     */
    public changeUserRole(userName: string, newRole: string) {
        let isRoleChanges = false;
        /* Проверка на то, что новая роль - действительно новая, а не текущая */
        for (let user of this.users) {
            if (user.name === userName && user.role !== newRole) {
                isRoleChanges = true;
                break;
            }
        }
        if (isRoleChanges) {
            this.permissionService.changeUserRole(userName, newRole).then((result) => {
                if (result) {
                    this.setupUsers().then();
                }
            });
        }
    }

    /**
     * Метод определяет выбраную для редактирования роль. Вызывается в шаблоне при выборе роли для редактирования.
     * Шаблон передает в метод строку - название выбраной роли.
     *
     * @param roleName
     */
    public selectRole(roleName: string) {
        for (let role of this.roles) {
            if (role.name === roleName) {
                this.selectedRole = role;
                break;
            }
        }
    }

    /**
     * Метод удаления роли. Вызывается в шаблоне при попытке удаления роли.
     * Шаблон передает в метод строку - название удаляемой роли.
     *
     * @param role
     */
    public deleteRole(role: string) {
        if (this.selectedRole && this.selectedRole.name === role) {
            this.selectedRole = null;
        }
        this.permissionService.deleteRole(role).then(() => {
            this.setupRoles().then(() => {
                this.setupResources().then();
            });
        });
    }

    /**
     * Метод создания роли. Вызывается в шаблоне при отправке формы создания роли.
     * Ипользует объект this.newRole в качестве данных для создания роли.
     */
    public createRole() {
        if (this.newRole.name) {
            this.permissionService.createRole(this.newRole.name).then(() => {
                this.setupRoles().then(() => {
                    this.setupResources().then();
                    this.newRole = new Role;
                    this.createRoleErrorMessage = null;
                });
            }).catch((err) => {
                this.createRoleErrorMessage = err;
            });
        } else {
            this.createRoleErrorMessage = 'All fields is required';
        }

    }

    /**
     * Метод проверяет статус - доступ задан\доступ не задан,
     * для построения интерфейса (чекбоксов) шаблона, при редактировании доступов
     * выбраной роли.
     * Шаблон передает название ресурса для проверки. В проверке учавствует объект this.selectedRole.
     *
     * @param resource{string}
     * @returns {boolean}
     */
    public isAllowed(resource: string): boolean {
        if (this.selectedRole.permissions) {
            for (let permission of this.selectedRole.permissions) {
                if (permission.resource === resource && permission.allowed) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Метод задает\уберает доступ для пользователя к определенному ресурсу.
     * Шаблон передает название роли и название ресурса для выполнения переключения доступа.
     *
     * @param roleName{string}
     * @param resource{string}
     */
    public togglePermission(roleName: string, resource: string) {
        this.permissionService.togglePermission(this.selectedRole.name, resource).then((isOk) => {
            if (isOk) {
                this.setupRoles().then(() => {
                    this.setupResources().then(() => {
                        this.selectRole(roleName);
                    });
                });
            }
        });
    }
}
