/**
 * Модель, определяющая свойства объекта User.
 */
export class User {
    /**
     * Имя пользователя.
     */
    name: string;

    /**
     * Роль пользователя.
     */
    role: string;

    /**
     * Пароль пользователя.
     */
    password: string;

    email?: string;
}
