/**
 * Модель, определяющая свойства для объекта Resource.
 */
export class Resource {
    /**
     * Название ресурса.
     */
    name: string;
}
