import {Injectable} from '@angular/core';
import {User} from './permission/user.model';
import {Role} from './permission/role.model';
import {Resource} from './permission/resource.model';
import {Route, Router} from '@angular/router';
import {Permission} from './permission/permission.model';
import {Headers, Http} from '@angular/http';

/**
 * Сервис, реализующий непосредственное взаимодействие с сервером
 * для модуля Permission
 */
@Injectable()
export class PermissionService {
    /**
     * Метод формирует из массива ролей и массива доступов, полученых с сервера,
     * массив ролей с привязанными к ним доступами, для удобного использования в компоненте.
     *
     * @param roles
     * @param permissions
     * @returns {Role[]}
     */
    private static combineRolesPermissions(roles: Role[], permissions: Permission[]): Role[] {
        for (let item in permissions) {
            if (permissions.hasOwnProperty(item) && permissions[item] !== null) {
                if (roles.hasOwnProperty(permissions[item].role)) {
                    if (roles[permissions[item].role] === null) {
                        roles[permissions[item].role] = [];
                    }
                    roles[permissions[item].role].push(permissions[item]);
                }
            }
        }
        return roles;
    }

    /**
     * Inject зависимостей.
     *
     * @param router
     * @param http
     */
    constructor(private router: Router, private http: Http) {}

    /**
     * Метод отправляет запрос на сервер и
     * возвращает всех пользователей.
     *
     * @returns {Promise<User[]>}
     */
    public getUsers(): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.http.get('/api/acl/user').subscribe((res) => {
                resolve(res.json());
            });
        });
    }

    /**
     * Метод отправляет запрос на создание нового пользователя,
     * возвращает созданого пользователя.
     *
     * @param user
     * @returns {Promise<User[]>}
     */
    public createUser(user: User): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.http.post(
                '/api/acl/user',
                user
            ).subscribe((res) => {
                if (res.json().error) {
                    reject(res.json().error);
                } else {
                    resolve(res.json());
                }
            });
        });
    }

    /**
     * Метод отправляет запрос на удаление определенного пользователя.
     * Возвращает удаленного пользователя.
     *
     * @param user
     * @returns {Promise<User[]>}
     */
    public deleteUser(user: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.http.delete(
                '/api/acl/user/' + user,
            ).subscribe((res) => {
                if (res.json().error) {
                    reject(res.json().error);
                } else {
                    resolve(res.json());
                }
            });
        });
    }

    /**
     * Метод возвращает (запрашиваемые с сервера) роли,
     * с привязанными к нип настройками доступов, в удобном для использования
     * в компоненте виде.
     *
     * @returns {Promise<Role[]>}
     */
    public getRolesPermissions(): Promise<Role[]> {
        return new Promise((resolve, reject) => {
            this.getRoles().then((roles) => {
                this.getPermissions().then((permissions) => {
                    resolve(PermissionService.combineRolesPermissions(roles, permissions));
                });
            });
        });
    }

    /**
     * Метод запрашивает с сервера и возвращает массив ролей.
     *
     * @returns {Promise<Role[]>}
     */
    private getRoles(): Promise<Role[]> {
        return new Promise((resolve, reject) => {
            this.http.get('/api/acl/role').subscribe((roles) => {
                resolve(roles.json());
            });
        });
    }

    /**
     * Метод запрашивает с сервера и возвращает массив пермиций.
     *
     * @returns {Promise<Permission[]>}
     */
    private getPermissions(): Promise<Permission[]> {
        return new Promise((resolve, reject) => {
            this.http.get('/api/acl/permission').subscribe((permissions) => {
                resolve(permissions.json());
            });
        });
    }

    /**
     * Метод отправляет запрос на сервер, на создание новой роли.
     * Возвращает результат добавления - true если добавлено, false если что-то пошло не так.
     *
     * @param role
     * @returns {Promise<boolean>}
     */
    public createRole(role: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.post(
                '/api/acl/role',
                {role: role}
            ).subscribe((res) => {
                if (res.json().error) {
                    reject(res.json().error);
                } else {
                    resolve(res.json());
                }
            });
        });
    }

    /**
     * Метод отправляет на сервер запрос, на удаление роли.
     * Возвращает результат удаления - true если роль удалена, false если что-то пошло не так.
     *
     * @param role
     * @returns {Promise<boolean>}
     */
    public deleteRole(role: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.delete(
                '/api/acl/role/' + role,
            ).subscribe(() => {
                resolve(true);
            });
        });
    }

    /**
     * Метод задает роль для пользователя.
     *
     * @param user
     * @param role
     * @returns {Promise<User[]>}
     */
    public changeUserRole(user: string, role: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.http.patch(
                '/api/acl/user',
                {name: user, role: role}
            ).subscribe((res) => {
                if (res.json().error) {
                    reject(res.json().error);
                } else {
                    resolve(res.json());
                }
            });
        });
    }

    /**
     * Метод возвращает массив ресурсов, для которых предусмотрено
     * конфигурирование доступов.
     *
     * @returns {Promise<Resource[]>}
     */
    public getResources(): Promise<Resource[]> {
        return new Promise((resolve, reject) => {
            let resources = this.parseRoutes();
            // this.updateServerResources(resources); TODO: create and update server-side resources data
            resolve(resources);
        });
    }

    /**
     * Метод парсит конфигурацию angular объекта Router,
     * и возвращает массив ресурсов, для которых предусмотрена настройка доступа.
     *
     * @param routesConfig
     * @param resources
     * @returns {Resource[]}
     */
    private parseRoutes(routesConfig: Route[] = this.router.config, resources: Resource[] = []): Resource[] {
        for (let route of routesConfig) {
            if (route.component && route.path) {
                if (route.canActivate) {
                    resources.push({name: route.path});
                }
                if (route.children) {
                    resources = this.parseRoutes(route.children, resources);
                }
            }
        }
        return resources;
    }

    /**
     * (пока не используется)
     * Метод обновляет на сервере данные о ресурсах.
     * TODO: реализовать обновление данных о ресурсах на сервере.
     *
     * @param resources
     */
    private updateServerResources(resources: Resource[]): void {
        this.http.patch('/api/acl/resource', {resources: JSON.stringify(resources)}).subscribe();
    }

    /**
     * Метод включатет\отключает доступ роли к ресурсу.
     *
     * @param role
     * @param resource
     * @returns {Promise<boolean>}
     */
    public togglePermission(role: string, resource: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.patch(
                '/api/acl/permission/' + role + '/' + resource, {}
                ).subscribe(() => {
                resolve(true);
            });
        });
    }
}
