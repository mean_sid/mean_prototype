import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PermissionComponent} from './permission/permission.component';

import {MaterialModule, MdNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        MaterialModule,
        MdNativeDateModule,
        FormsModule
    ],
    declarations: [
        PermissionComponent,
    ],
    exports: [PermissionComponent]
})
export class PermissionModule {

}
