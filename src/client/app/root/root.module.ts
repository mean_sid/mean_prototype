import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule, MdNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {ChatModule} from '../chat/chat.module';
import {RootComponent} from './root/root.component';
import {SortableModule} from '../sortable/sortable.module';
import {MainComponent} from './main/main.component';
import {ContentComponent} from './content/content.component';
import {CentralAreaComponent} from './central-area/central-area.component';
import {NavComponent} from './nav/nav.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {FooterComponent} from './footer/footer.component';
import {SocketModule} from '../socket/socket.module';
import {RootRoutingModule} from './root-routing.module';
import {PermissionModule} from '../permission/permission.module';
import {TestComponent} from './test/test.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
    declarations: [
        RootComponent,
        MainComponent,
        ContentComponent,
        CentralAreaComponent,
        NavComponent,
        SidebarComponent,
        FooterComponent,
        TestComponent,
        DashboardComponent,
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        MaterialModule,
        MdNativeDateModule,
        SortableModule,
        ChatModule,
        SocketModule,
        PermissionModule,
        RootRoutingModule
    ],
    providers: [],
    exports: [RootComponent]
})
export class RootModule {
}
