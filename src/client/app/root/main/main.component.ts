import {Component} from '@angular/core';

/**
 * Основной компонент, определяющий подключение частей шаблона,
 * таких как хедер, футер и центральная часть.
 */
@Component({
    selector: 'app-root-component',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.css']
})
export class MainComponent {}
