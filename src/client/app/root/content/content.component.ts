import {Component} from '@angular/core';

/**
 * Компонент центральной основной области приложения.
 * Содержит router-outlet для роутинга основных компонентов приложения.
 */
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent {}
