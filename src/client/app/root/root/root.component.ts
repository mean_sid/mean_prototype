import {Component} from '@angular/core';

/**
 * Корень основной части приложения. Содержит router-outlet для
 * роутинга на MainComponent (или на любой другой шаблон в будущем).
 */
@Component({
    selector: 'app-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.css']
})
export class RootComponent {}
