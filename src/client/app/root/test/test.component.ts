import {Component} from '@angular/core';

/**
 * Тестовый компонент (подключен в сайдбаре).
 */
@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent {

    public send() {

    }
}
