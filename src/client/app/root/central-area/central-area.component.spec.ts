import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CentralAreaComponent} from './central-area.component';
import {ContentComponent} from '../content/content.component';
import {SidebarComponent} from '../sidebar/sidebar.component';
import {RouterTestingModule} from '@angular/router/testing';
import {RootRoutingModule} from '../root-routing.module';
import {RootModule} from '../root.module';

describe('CentralAreaComponent', () => {
    let component: CentralAreaComponent;
    let fixture: ComponentFixture<CentralAreaComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [RouterTestingModule, RootModule],
            providers: []
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CentralAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
