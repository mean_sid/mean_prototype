import {Component} from '@angular/core';

/**
 * Компонент центральной области приложения. Содержит компоненты Content и Sidebar.
 */
@Component({
  selector: 'app-central-area',
  templateUrl: './central-area.component.html',
  styleUrls: ['./central-area.component.css']
})
export class CentralAreaComponent {}
