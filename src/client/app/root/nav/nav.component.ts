import {Component} from '@angular/core';

/**
 * Компонент, содержащий хедер и навигацию приложения.
 */
@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent {}
