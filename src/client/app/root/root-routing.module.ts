import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SortableComponent} from '../sortable/sortable/sortable.component';
import {PermissionComponent} from '../permission/permission/permission.component';
import {SocketComponent} from '../socket/socket.component';
import {ChatComponent} from '../chat/chat/chat.component';
import {MainComponent} from './main/main.component';
import {AuthGuardService} from '../authorization/auth-guard.service';
import {DashboardComponent} from './dashboard/dashboard.component';

/*
    Експорт настроек роутинга для управления доступами
*/
export const rootRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'root'
    },
    {
        path: 'root',
        component: MainComponent,
        canActivate: [AuthGuardService],

        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'sortable',
                component: SortableComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'permission',
                component: PermissionComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'socket',
                component: SocketComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'chat',
                component: ChatComponent,
                canActivate: [AuthGuardService]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(rootRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class RootRoutingModule {
}
