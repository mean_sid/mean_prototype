import {Injectable} from '@angular/core';

/**
 * Сервис для работы с серверами по протоколу WebSocket.
 */
@Injectable()
export class SocketService {
    /**
     * Адрес сервера и порта для соединения с сокет сервером, который крутится на эмуляторе linkit'а
     * TODO: убрать из клиента все взаимодействие с сокетами Tera!
     * @type {string}
     */
    private tera = 'ws://192.168.50.100:7681';

    /**
     * Адрес и порт сервера, реализующего сокет соединение для работы модуля Chat
     *
     * @type {string}
     */
    private nodeSocketUrl = 'ws://' + window.location.hostname + ':9082';

    /**
     * Основной метод, создающий соединение по протоколу WebSocket.
     * Принимает условное название сервера и опциональный параметр - protocol.
     * Возвращает объект соединения WebSocket.
     *
     * @param server
     * @param protocol
     * @returns {WebSocket}
     */
    public create(server?: string, protocol: string|null = null): WebSocket {
        let url: string;

        switch (server) {
            case 'tera':
                url = this.tera;
                break;
            case 'node':
                url = this.nodeSocketUrl;
                break;
            default :
                url = this.nodeSocketUrl;
        }

        return new WebSocket(url, protocol);
    }
}

