import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SocketComponent} from './socket.component';
import {SocketService} from './socket.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [SocketComponent],
    exports: [SocketComponent]
})
export class SocketModule {
}
