import {Component, OnInit} from '@angular/core';
import {SocketService} from './socket.service';

/**
 * Демонстрационный компонетн, свзязующийся с сервером, запущеным на эмуляторе linkit'a,
 * по протоколу WebSocket.
 */
@Component({
    selector: 'app-socket',
    templateUrl: './socket.component.html',
    styleUrls: ['./socket.component.css']
})
export class SocketComponent implements OnInit {
    /**
     * Объект сокет - соединения. Отображает счетчик.
     */
    private socketCounter: WebSocket;

    /**
     * Объект сокет - соединения. Отображает данные сервера.
     */
    private socketStatus: WebSocket;

    /**
     * Объект хранящий данные, которые передает сервер черес соединение socketStatus
     */
    public status: any;

    /**
     * Показатель счетчика, передаваемого сервером через соединение socketCounter
     *
     * @type {number}
     */
    public counter = 0;

    /**
     * Текущий статус соединения socketCounter
     */
    public socketCounterInfo: any;

    /**
     * Текущий статус соединения socketStatus
     */
    public socketStatusInfo: any;

    /**
     * Inject зависимостей.
     *
     * @param socketService
     */
    constructor (private socketService: SocketService) {}

    /**
     * Создание и конфигурирование объектов сокет - соединений.
     */
    ngOnInit() {
        this.socketCounter = this.socketService.create('tera', 'dumb-increment-protocol');
        this.socketStatus = this.socketService.create('tera', 'lws-status');

        this.socketCounterInfo = this.socketCounter.readyState;
        this.socketStatusInfo = this.socketStatus.readyState;

        this.socketCounter.onopen = (message: any) => {
            this.socketCounterInfo = this.socketCounter.readyState;
        };

        this.socketStatus.onopen = (message: any) => {
            this.socketStatusInfo = this.socketStatus.readyState;
        };

        this.socketCounter.onmessage = (message: any) => {
            this.showCounter(message);
        };

        this.socketStatus.onmessage = (message: any) => {
            this.showStatus(message);
        };

        this.socketCounter.onerror = (event) => {
            this.socketOnError(event);
        };

        this.socketStatus.onerror = (event) => {
            this.socketOnError(event);
        };
    }

    /**
     * Метод обработки ошибки сокет - соединения.
     *
     * @param event
     */
    private socketOnError(event: Event) {
        console.log(event);
    }

    /**
     * Метод отображающий новое значение счетчика.
     *
     * @param data
     */
    showCounter(data: any) {
        this.counter = data.data;
    }

    /**
     * Метод обновляет объект this.status когда приходят новые данные.
     *
     * @param data
     */
    showStatus(data: any) {
        this.status = JSON.parse(data.data);
    }
}
