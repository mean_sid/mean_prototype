import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RootComponent} from './root/root/root.component';
import {AuthorizationComponent} from './authorization/authorization/authorization.component';

/*
    Експорт настроек роутинга для управления доступами
*/
export const appRoutes: Routes = [
    {path: '', pathMatch: 'full', component: RootComponent/*, canActivate: [AuthGuardService]*/},
    {path: 'auth', component: AuthorizationComponent},
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
