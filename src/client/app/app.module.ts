import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import {RootModule} from './root/root.module';
import {SocketService} from './socket/socket.service';
import {AppRoutingModule} from './app-routing.module';
import {PermissionService} from './permission/permission.service';
import {AuthorizationModule} from './authorization/authorization.module';
import {AuthGuardService} from './authorization/auth-guard.service';
import {AuthService} from './authorization/auth.service';
import {CookieService} from 'angular2-cookie/core';
import {AlertComponent} from './_directives/alert.component';
import {AlertService} from './_services/alert.service';

@NgModule({
    declarations: [
        AppComponent,
        AlertComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AuthorizationModule,
        RootModule,

        AppRoutingModule,
    ],
    providers: [
        SocketService,
        AuthGuardService,
        AuthService,
        PermissionService,
        {provide: CookieService, useFactory: cookieServiceFactory},
        AlertService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
/* Фикс ошибки, которую выдает модуль angular-cookie при production компиляции через angular-cli */
export function cookieServiceFactory() { return new CookieService(); }
