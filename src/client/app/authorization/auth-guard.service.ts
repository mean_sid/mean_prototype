import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {CookieService} from 'angular2-cookie/services/cookies.service';
import {Http} from '@angular/http';
import {AlertService} from '../_services/alert.service';

/**
 * Сервис, реализующий интерфейс ангуляра CanActivate,
 * используется при определении защищенных роутов.
 */
@Injectable()
export class AuthGuardService implements CanActivate {
    /**
     * Inject зависимостей.
     *
     * @param authService
     * @param router
     * @param cookie
     */
    constructor(
        private authService: AuthService,
        private router: Router,
        private cookie: CookieService,
        private alertService: AlertService
    ) {}

    /**
     * Метод вызывается ангуляром при проверке защищенных роутов,
     * проверяет доступность роута пользователю
     *
     * @param rout
     * @param state
     * @returns {Promise<boolean>}
     */
    canActivate(rout: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.cookie.get('userToken')) {
                this.authService.checkPermission(rout.url[0].path).then((response) => {
                    if (!response.allowed) {
                        let alertMsg = 'Permission denied.';
                        if (response.error) {
                            alertMsg += ' Reason: ' + response.error;
                        }
                        this.router.navigate(['/auth']);
                        this.alertService.error(alertMsg, true);
                    }
                    resolve(response.allowed);
                }).catch((error) => {
                    this.alertService.error('Permission denied');
                    resolve(false);
                });
            } else {
                this.alertService.error('Login required', true);
                this.authService.redirectUrl = state.url;
                this.router.navigate(['/auth']);
                resolve(false);
            }
        });
    }
}
