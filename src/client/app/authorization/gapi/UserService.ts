import {Injectable, NgZone} from '@angular/core';
import * as _ from 'lodash';
import {GoogleAuthService} from './index';
import GoogleUser = gapi.auth2.GoogleUser;
import GoogleAuth = gapi.auth2.GoogleAuth;

@Injectable()
export class UserService {
    public static readonly SESSION_STORAGE_KEY: string = 'googleAccessToken';
    private user: GoogleUser = undefined;

    public static getToken() {
        return sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
    }

    constructor(private googleAuthService: GoogleAuthService,
                private ngZone: NgZone) {
    }

    public setUser(user: GoogleUser): void {
        this.user = user;
    }

    public getCurrentUser(): GoogleUser {
        return this.user;
    }

    public getToken(): string {
        let token: string = sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
        if (!token) {
            throw new Error('no token set , authentication required');
        }
        return sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
    }

    public signIn(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.googleAuthService.getAuth().subscribe((auth) => {
                auth.signIn().then((res: any) => {
                    this.ngZone.run(() => {
                        this.user = res;
                        sessionStorage.setItem(
                            UserService.SESSION_STORAGE_KEY, res.getAuthResponse(true).access_token
                        );
                        resolve(res);
                    });

                }, (err: any) => this.signInErrorHandler(err));
            });
        });

    }

    public signOut(): void {
        this.googleAuthService.getAuth().subscribe((auth) => {
            try {
                auth.signOut();
            } catch (e) {
                console.error(e);
            }
            sessionStorage.removeItem(UserService.SESSION_STORAGE_KEY);
        });
    }

    public isUserSignedIn(): boolean {
        return !_.isEmpty(sessionStorage.getItem(UserService.SESSION_STORAGE_KEY));
    }

    private signInSuccessHandler(res: GoogleUser) {
        this.ngZone.run(() => {
            this.user = res;
            sessionStorage.setItem(
                UserService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token
            );
        });
    }

    private signInErrorHandler(err: any) {
        console.warn(err);
    }
}