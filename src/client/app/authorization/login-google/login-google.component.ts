import {Component, OnInit} from '@angular/core';
import {UserService} from '../gapi/UserService';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
    selector: 'app-login-google',
    templateUrl: './login-google.component.html',
    styleUrls: ['./login-google.component.css']
})
export class LoginGoogleComponent implements OnInit {

    /**
     * Сообщения об ошибках валидации.
     * Свойство связано с шаблоном.
     */
    public errorMessage: string;

    constructor(private userService: UserService, private router: Router, private authService: AuthService) {
    }

    ngOnInit() {
    }

    public login() {
        this.userService.signIn().then((res) => {
            this.loginToApp(res);
        });
    }

    private loginToApp(auth: any) {
        this.authService.loginByGoogle(auth).then((isLoggedIn) => {
            if (isLoggedIn) {
                this.errorMessage = '';
                this.router.navigate([this.authService.redirectUrl]).then();
            }
        }).catch((error) => {
            this.errorMessage = error;
        });
    }
}
