import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule, MdNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';

import {FormsModule} from '@angular/forms';
import {AuthorizationComponent} from './authorization/authorization.component';
import {LogoutComponent} from './logout/logout.component';
import {LoginAppComponent} from './login-app/login-app.component';
import {LoginGoogleComponent} from './login-google/login-google.component';
import {GoogleApiModule} from './gapi/GoogleApiModule';
import {NG_GAPI_CONFIG, ClientConfig} from './gapi/index';

let gapiClientConfig: ClientConfig = {
    clientId: '891167589816-ifftmfvdbshc1k9rk5hu219gltc05f3d.apps.googleusercontent.com',
    discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
    scope: [
        'https://www.googleapis.com/auth/analytics.readonly',
        'https://www.googleapis.com/auth/analytics'
    ].join(' ')
};
@NgModule({
    declarations: [
        AuthorizationComponent,
        LogoutComponent,
        LoginAppComponent,
        LoginGoogleComponent,
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        MaterialModule,
        MdNativeDateModule,
        FormsModule,
        RouterModule,
        GoogleApiModule.forRoot({
            provide: NG_GAPI_CONFIG,
            useValue: gapiClientConfig
        }),
    ],
    providers: [],
    exports: [AuthorizationComponent]
})
export class AuthorizationModule {

}
