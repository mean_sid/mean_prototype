import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../auth.service';
import {UserService} from '../gapi/UserService';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
    @Output() onLogout = new EventEmitter<boolean>();
    @Input('type') type: string;

    public isLoggedInState: boolean;

    constructor(
        private authService: AuthService,
        private userService: UserService
    ) {
        if (this.type === 'google') {

        }
    }

    ngOnInit() {
    }


    /**
     * Выход пользователя.
     */
    public logout() {
        switch (this.type) {
            case 'app':
                this.logoutApp();
                break;
            case 'google':
                this.logoutGoogle();
                break;
        }

    }

    private logoutApp() {
        this.authService.logout();
        this.isLoggedInState = this.isLoggedIn();
        this.onLogout.emit(this.isLoggedInState);
    }

    private logoutGoogle() {
        this.userService.signOut();
        this.isLoggedInState = this.userService.isUserSignedIn();
        this.onLogout.emit(this.isLoggedInState);
    }

    /**
     * Проверка - авторизован ли пользователь.
     *
     * @returns {boolean}
     */
    public isLoggedIn() {
        return this.authService.isLoggedIn();
    }

}
