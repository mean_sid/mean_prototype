import {AfterViewInit, Component, Input, OnChanges, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {UserService} from '../gapi/UserService';
import {GoogleAuthService} from '../gapi/GoogleAuthService';
import {GoogleApiService} from '../gapi/GoogleApiService';

@Component({
    selector: 'app-login-app',
    templateUrl: './login-app.component.html',
    styleUrls: ['./login-app.component.css']
})
export class LoginAppComponent implements OnInit, OnChanges {
    /**
     * Вводимый пользователем логин.
     * Свойство связано с инпутом шаблона.
     */
    public name: string;

    /**
     * Вводимый пользователем пароль.
     * Свойство связано с инпутом шаблона.
     */
    public password: string;

    /**
     * Сообщения об ошибках валидации.
     * Свойство связано с шаблоном.
     */
    public errorMessage: string;

    constructor(
        private authService: AuthService,
        private router: Router,
        private userService: UserService,
        private googleAuthService: GoogleAuthService,
        private googleApiService: GoogleApiService
    ) {

    }

    ngOnInit() {
    }

    ngOnChanges() {}


    /**
     * Авторизация пользователя.
     */
    public login() {
        this.authService.login(this.name, this.password).then((isLoggedIn) => {
            if (isLoggedIn) {
                this.errorMessage = '';
                this.router.navigate([this.authService.redirectUrl]).then();
            }
        }).catch((error) => {
            this.errorMessage = error;
        });
    }

    // public setupGoogleUser() {
    //     this.getGoogleUser().then((user) => {
    //         this.googleUser = {
    //             name: user.getName(),
    //             id: user.getId(),
    //             email: user.getEmail()
    //         };
    //     });
    // }
    //
    // private getGoogleUser(): Promise<any> {
    //     return new Promise((resolve, reject) => {
    //         this.googleApiService.onLoad(() => {
    //             this.googleAuthService.getAuth().subscribe((auth) => {
    //                 auth.currentUser.listen((res) => {
    //                     resolve(res.getBasicProfile());
    //                 });
    //             });
    //         });
    //
    //     });
    // }
    //
    // public googleLogout() {
    //     this.userService.signOut();
    // }

}
