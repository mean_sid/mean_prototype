import {Injectable} from '@angular/core';
import {ajaxGet, ajaxPost} from 'rxjs/observable/dom/AjaxObservable';
import {CookieService} from 'angular2-cookie/services/cookies.service';
import {Http} from '@angular/http';
import {UserService} from './gapi/UserService';

/**
 * Сервис, содержащий логику авторизации.
 */
@Injectable()
export class AuthService {
    /**
     * Свойство хранит ссылку, запрошеную пользователем, для последующего
     * редиректа после авторизации.
     *
     * @type {string}
     */
    public redirectUrl = '/';

    /**
     * Определяется приватное свойство cookie = CookieService
     *
     * @param cookie
     * @param http
     */
    constructor(private cookie: CookieService, private http: Http) {}

    /**
     * Проверка доступа пользователя к компоненту.
     * Вызывается AuthGuard сервисом.
     *
     * @param component name
     * @returns {Promise<boolean>}
     */
    public checkPermission(component: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let data = {
                token: this.cookie.get('userToken'),
                resource: component,
                googleToken: ''
            };

            let googleToken = UserService.getToken();
            if (googleToken) {
                data.googleToken = googleToken;
            }

            return ajaxGet(
                '/auth/check',
                {data: JSON.stringify(data)}
            ).subscribe((res) => { resolve(res.response); });
        });
    }

    /**
     * Метод авторизации. Устанавливает cookie, хранящую токен
     * пользователя.
     *
     * @param user User name
     * @param pass User password
     * @returns {Promise<boolean|string>}
     */
    public login(user: string, pass: string): Promise<boolean|string> {
        return new Promise((resolve, reject) => {
            this.tryToLogin(user, pass).then((token) => {
                this.cookie.put('userToken', token, {path: '/'});
                resolve(true);
            }).catch((err: any) => {
                reject(err);
            });
        });
    }


    public loginByGoogle(auth: any): Promise<boolean|string> {
        return new Promise((resolve, reject) => {
            this.tryToLoginByGoogle(auth).then((token) => {
                this.cookie.put('userToken', token, {path: '/'});
                resolve(true);
            }).catch((err: any) => {
                reject(err);
            });
        });
    }

    /**
     * Метод содержащий логику авторизации и взаимодействие с сервером.
     *
     * @param user
     * @param pass
     * @returns {Promise<string>}
     */
    private tryToLogin(user: string, pass: string): Promise<string> {
        return new Promise((resolve, reject) => {
            ajaxPost('/auth/login', {user: JSON.stringify({name: user, password: pass})}).subscribe(
                (res) => {
                    if (res.response.hasOwnProperty('error')) {
                       reject(res.response.error);
                    } else {
                        resolve(res.response);
                    }
                },
                (err: any) => { reject(err); }
            );
        });
    }


    private tryToLoginByGoogle(auth: any): Promise<string> {
        return new Promise((resolve, reject) => {
            let user = auth.getBasicProfile();
            ajaxPost('/auth/login/google', {user: JSON.stringify(
                    {
                        token: sessionStorage.getItem(UserService.SESSION_STORAGE_KEY)
                    }
                )}).subscribe(
                (res) => {
                    if (res.response.hasOwnProperty('error')) {
                       reject(res.response.error);
                    } else {
                        resolve(res.response);
                    }
                },
                (err: any) => { reject(err); }
            );
        });
    }

    public logout() {
        this.cookie.remove('userToken');
    }

    public isLoggedIn() {
        return !!this.cookie.get('userToken');
    }

    /**
     * Метод просит сервер проверить пользователя, на локальное либо глобальное подключение.
     *
     * @returns {Promise<boolean>}
     */
    public isLocalUser(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.get('/auth/check/isLocalUserCheck').subscribe((res) => {
                resolve(res.json().answer);
            });
        });
    }
}
