import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/services/cookies.service';
import {UserService} from '../gapi/UserService';

/**
 * Основной компонент авторизации.
 * Отображает и обрабатывает интерфейс авторизации.
 */
@Component({
    selector: 'app-authorization',
    templateUrl: './authorization.component.html',
    styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {


    public isLoggedInState: boolean;
    public isGoogleLoggedInState = false;

    public isLocalUser: boolean;

    /**
     * Inject зависимостей.
     *
     * @param authService
     * @param router
     * @param userService
     */
    constructor(
        public authService: AuthService,
        public router: Router,
        private userService: UserService
    ) {}

    ngOnInit() {
        this.isLoggedInState = this.isLoggedIn();
        this.authService.isLocalUser().then((res) => {
            this.isLocalUser = res;
        });
    }

    /**
     * Проверка - авторизован ли пользователь.
     *
     * @returns {boolean}
     */
    public isLoggedIn() {
        return this.authService.isLoggedIn();
    }


    public isGoogleLoggedIn() {
        return this.userService.isUserSignedIn();
    }

    public onLogout(state: boolean) {
        this.isLoggedInState = state;
    }
}
