import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthorizationComponent} from './authorization.component';
import {MaterialModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {AuthService} from '../auth.service';
import {CookieService} from 'angular2-cookie/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Router} from '@angular/router';
import {By} from '@angular/platform-browser';

describe('AuthorizationComponent', () => {
    let component: AuthorizationComponent;
    let fixture: ComponentFixture<AuthorizationComponent>;
    let authService: AuthService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AuthorizationComponent],
            imports: [MaterialModule, FormsModule, NoopAnimationsModule],
            providers: [AuthService, CookieService, {
                provide: Router, useClass: class {
                    navigate = () => {};
                }
            }]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AuthorizationComponent);
        component = fixture.componentInstance;
        authService = fixture.debugElement.injector.get(AuthService);
    });

    it('should be created', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('component for login displayed correctly', () => {
        spyOn(authService, 'isLoggedIn').and.returnValue(false);

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let loginInputElement = fixture.debugElement.query(By.css('input[name="login"]'));
            expect(loginInputElement).toBeTruthy();
        });
    });

    it('component for logout displayed correctly', () => {
        spyOn(authService, 'isLoggedIn').and.returnValue(true);

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(component.isLoggedInState).toBeTruthy();

            let loginInputElement = fixture.debugElement.query(By.css('input[name="login"]'));
            expect(loginInputElement).toBeFalsy();

            let title = fixture.debugElement.query(By.css('h1')).nativeElement.innerText;
            expect(title).toEqual('LOGOUT');
        });
    });

    // it('logout works as expected', () => {
    //     spyOn(authService, 'isLoggedIn').and.returnValue(true);
    //     let spy = spyOn(authService, 'logout').and.returnValue(null);
    //
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //
    //         let logoutBtn = fixture.debugElement.query(By.css('.logout-btn')).nativeElement;
    //         expect(logoutBtn).toBeTruthy();
    //
    //         component.logout();
    //
    //         expect(spy.calls.count()).toEqual(1);
    //     });
    // });

    // it('login works as expected', () => {
    //     spyOn(authService, 'isLoggedIn').and.returnValue(false);
    //     let spy = spyOn(authService, 'login').and.callFake((name: string, pass: string) => {
    //         return new Promise((resolve, reject) => {
    //             if (name === 'test login' && pass === 'test pass') {
    //                 resolve(true);
    //             } else {
    //                 reject('test login error');
    //             }
    //         });
    //     });
    //     component.login();
    //     fixture.detectChanges();
    //
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //
    //         expect(spy.calls.count()).toEqual(1);
    //         expect(component.errorMessage).toEqual('test login error');
    //
    //         let displayedErrorMessage = fixture.debugElement.query(By.css('.error-message')).nativeElement.innerText;
    //         expect(displayedErrorMessage).toEqual('test login error');
    //
    //     });
    // });

    // it('login works as expected', () => {
    //     spyOn(authService, 'isLoggedIn').and.returnValue(false);
    //     let router = fixture.debugElement.injector.get(Router);
    //     let spyNavigate = spyOn(router, 'navigate').and.returnValue(Promise.resolve());
    //     let spyLogin = spyOn(authService, 'login').and.callFake((name: string, pass: string) => {
    //         return new Promise((resolve, reject) => {
    //             if (name === 'test login' && pass === 'test pass') {
    //                 resolve(true);
    //             } else {
    //                 reject('test login error');
    //             }
    //         });
    //     });
    //
    //     component.name = 'test login';
    //     component.password = 'test pass';
    //
    //     component.login();
    //     fixture.detectChanges();
    //
    //     fixture.whenStable().then(() => {
    //         fixture.detectChanges();
    //
    //         expect(spyLogin.calls.count()).toEqual(1);
    //         expect(component.errorMessage).toBeFalsy();
    //
    //         let displayedErrorMessage = fixture.debugElement.query(By.css('.error-message'));
    //         expect(displayedErrorMessage).toBeFalsy();
    //
    //         expect(spyNavigate.calls.count()).toEqual(1);
    //     });
    // });
});
