import {SortableItem} from '../../classes/models/SortableItem';

let items: Array<SortableItem> = [
    {id: 6, name: 'Device 6'},
    {id: 7, name: 'Device 7'},
    {id: 8, name: 'Device 8'},
    {id: 9, name: 'Device 9'},
    {id: 10, name: 'Device 10'},
];

export = items;
