import {Acl} from './models/Acl';
import {User} from './models/User';
import {Encryptor} from './Encryptor';
import {Server} from './Server';
import {clientProtocolAccess} from '../environment/config';
declare let Promise: any;

/**
 *  Класс реализующий проверку пользователя и его токена
 */
export class Guard {
    /** синглтон текущего класса */
    private static _instance: Guard;
    /** acl - синглтон-объект, через который ведется работа с данными ролей, ресурсов и доступов */
    private static acl: Acl;

    /** Основной метод, возвращающий экземпляр класса */
    public static instance() {
        if (!this._instance) {
            this._instance = new this;
            this.acl = Acl.instance();
        }

        return Guard._instance;
    }

    public static isLocalIp(ip) {
        let regexp = new RegExp('(^127\.0\.0\.1$|^::ffff:127\.0\.0\.1$|^::1$|^::ffff:192\.168.*$)');
        let result = regexp.exec(ip);

        return result !== null;
    }


    /** Приватный конструктор делает невозможным создание экземпляра класса вне класса */
    private constructor() {}

    /**
     * Основной метод проверки доступа пользователя к ресурсу.
     * Принимает название-идентификатор ресурса,
     * и зашифрованый токен пользователя, полученый из cookie.
     * Возвращает Promise<boolean>, возвращающий результат проверки доступа.
     *
     * @param resource string название-идентификатор ресурска, для которого проходит проверка доступа.
     * @param userToken string зашифрованый токен, идентифицирующий пользователя.
     * @param isSocket boolean флаг, означающий что проверяется доступ к протоколу сокет-соединения, а не к обычному ресурсу
     */
    public isAllowedResource(resource: string, userToken: string, isSocket = false): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.checkAllowed({token: userToken, resource: resource, isSocket: isSocket}).then((allowed) => {
                resolve(allowed);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Вспомогательный внутренний метод, реализующий непосредственную проверку доступа.
     *
     * @param data Object {token:string, resource:string, isSocket:boolean}
     */
    private checkAllowed(data): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let decryptedUser = Encryptor.instance().decryptUser(data.token);
            User.get(decryptedUser.name).then((user) => {

                /** Если проверяется доступ к протоколу сокета */
                if (data.isSocket) {

                    let userRole = user.getRole().toLowerCase();

                    /** Если в конфиге доступов есть роль пользователя, и ей разрешен доступ к данному протоколу. */
                    if (
                        typeof clientProtocolAccess[userRole] !== 'undefined' &&
                        clientProtocolAccess[userRole].indexOf(data.resource) !== -1
                    ) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }

                } else {
                    /** Проверка на доступ к ресурсу */
                    Guard.acl.query(user.getRole(), data.resource, null).then((allowed) => {
                        resolve(allowed);
                    });
                }

            }).catch((err) => {
                reject(err);
            });
        });
    }
}
