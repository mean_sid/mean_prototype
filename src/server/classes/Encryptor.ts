import * as crypto from 'crypto';
import {User} from './models/User';

// TODO: добавить возбуждение и обработку ошибок при расшифровке строки, зашифрованной с использованием устаревшего пароля
export class Encryptor {
    /** синглтон текущего класса */
    private static _instance: Encryptor;

    /** Подключенная библиотека crypto */
    private crypto;
    private algorithm = 'aes-256-ctr';
    private password = 'hy27dyur47gds23itfj';  // TODO: реализовать авто-генерацию пароля

    public static instance(): Encryptor {
        if (!this._instance) {
            this._instance = new this;
        }
        return this._instance;
    }

    private constructor() {
        this.crypto = crypto;
    }

    public encrypt(text: string) {
        let cipher = this.crypto.createCipher(this.algorithm, this.password);
        let crypted = cipher.update(text, 'utf8', 'hex');
        crypted += cipher.final('hex');
        return crypted;
    }

    public decrypt(text: string) {
        let decipher = this.crypto.createDecipher(this.algorithm, this.password);
        let decrypted = decipher.update(text, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;
    }

    public encryptUser(user: User): string|boolean {
        if (!user.getName() || !user.getPassword()) {
            return false;
        }
        return this.encrypt(user.getName() + '|' + user.getPassword());
    }

    public decryptUser(token: string) {
        let decryptedToken = this.decrypt(token);
        let userData = decryptedToken.split('|');
        return {name: userData[0], password: userData[1]};
    }
}
