import {BaseRouter} from '../../BaseRouter';
import {Permission} from '../../../models/Permission';

export class PermissionRouter extends BaseRouter {
    protected setup() {

        this.router.get('', (req, res) => {
            Permission.getAll().then((permissions) => {
                res.json(permissions);
            }).catch();
        });

        this.router.patch('/:role/:resource', (req, res) => {
            Permission.togglePermission({
                role: req.params.role,
                resource: req.params.resource
            }).then((status) => {
                res.json(status);
            });
        });

        return super.setup();
    }
}

