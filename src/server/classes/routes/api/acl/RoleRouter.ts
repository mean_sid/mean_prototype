import {BaseRouter} from '../../BaseRouter';
import {Role} from '../../../models/Role';

export class RoleRouter extends BaseRouter {
    protected setup() {

        this.router.post('', (req, res) => {
            Role.getAll().then((roles) => {
                if (roles.hasOwnProperty(req.body.role)) {
                    res.json({error: 'Role already exists'});
                } else {
                    new Role(req.body.role).save().then((result) => {
                        res.json(result);
                    });
                }
            });
        });

        /**
         * @api {get} /api/acl/role Get all roles
         * @apiGroup Role
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "role": "role name"
         *    },
         *    {
         *      "id": 2,
         *      "role": "role name"
         *    }]
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        this.router.get('', (req, res) => {
            Role.getAll().then((roles) => {
                res.json(roles);
            });
        });

        this.router.delete('/:role', (req, res) => {
            Role.get(req.params.role).then((role) => {
                role.deleteRole().then((result) => {
                    res.json(result);
                }).catch((err) => {
                    res.json(err);
                });
            }).catch((err) => {
                res.json(err);
            });
        });

        return super.setup();
    }
}

