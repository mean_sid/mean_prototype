import {BaseRouter} from '../../BaseRouter';
import {User} from '../../../models/User';

/*
    Роутер реализует функционал управления пользователями с помощью UserModel класса
*/
export class UserRouter extends BaseRouter {
    protected setup() {
        
        /**
         * @api {post} /api/acl/user Create a user
         * @apiGroup User
         * @apiSuccess {Object[]} tasks Task's list
         * @apiSuccess {Number} tasks.id Task id
         * @apiSuccess {String} tasks.title Task title
         * @apiSuccess {Boolean} tasks.done Task is done?
         * @apiSuccess {Date} tasks.updated_at Update's date
         * @apiSuccess {Date} tasks.created_at Register's date
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "name": "Username",
         *      "password": "somePass",
         *      "role": "Admin"
         *    }]
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        this.router.post('', (req, res) => {
            let user = req.body;
            /** Проверка на существование создаваемого пользователя.*/
            User.get(user.name).then((user) => {
                res.json({error: 'User already exists'});
            }).catch(() => {
                new User(user.name, user.role, user.password, user.email)
                    .save()
                    .then((result) => {
                        res.json(result);
                    }
                );
            });
        });

        /**
         * @api {patch} /api/acl/user Update a user data
         * @apiGroup User
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "name": "newName",
         *      "password": "somePass",
         *      "role": "Admin"
         *    }]
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        this.router.patch('', (req, res) => {
            User.get(req.body.name).then((user) => {
                user.setRole(req.body.role).then((result) => {
                    res.json(result);
                });
            });
        });

        /**
         * @api {get} /api/acl/user Get all users
         * @apiGroup User
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "name": "username",
         *      "password": "somePass",
         *      "role": "Admin"
         *    },
         *    {
         *      "id": 2,
         *      "name": "username",
         *      "password": "somePass",
         *      "role": "User"
         *    }]
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        this.router.get('', (req, res) => {
            User.getAll(true).then((result) => {
                res.json(result);
            });
        });

        /**
         * @api {delete} /api/acl/user/:user Delete a user data
         * @apiGroup User
         * @apiSuccessExample {json} Success
         *    HTTP/1.1 200 OK
         *    [{
         *      "id": 1,
         *      "name": "DeletedUser",
         *      "password": "somePass",
         *      "role": "Admin"
         *    }]
         * @apiErrorExample {json} List error
         *    HTTP/1.1 500 Internal Server Error
         */
        this.router.delete('/:user', (req, res) => {
            User.get(req.params.user).then((user) => {
                user.deleteUser().then((result) => {
                    res.json(result);
                });
            });
        });

        return super.setup();
    }
}

