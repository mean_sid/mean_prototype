import {UserRouter} from './acl/UserRouter';
import {BaseRouter} from '../BaseRouter';
import {RoleRouter} from './acl/RoleRouter';
import {ResourceRouter} from './acl/ResourceRouter';
import {PermissionRouter} from './acl/PermissionRouter';

/**
 *  Корневой роутер для реализации Access Control List функционала
 */
export class AclRouter extends BaseRouter {
    protected resource = 'permission';

    protected setup () {

        this.router.use('/user', UserRouter.instance().get());
        this.router.use('/role', RoleRouter.instance().get());
        this.router.use('/resource', ResourceRouter.instance().get());
        this.router.use('/permission', PermissionRouter.instance().get());

        return super.setup();
    }
}
