import * as data from '../../../data/sortable/index';
import {BaseRouter} from '../BaseRouter';

export class SortableRouter extends BaseRouter {
    protected resource = 'sortable';

    protected setup () {
        this.router.get('/items/:id', (req, res) => {
            for (let i in data) {
                if (i === 'items' + req.params['id']) {
                    res.send(JSON.stringify(data[i]));
                }
            }
        });

        return super.setup();
    }
}
