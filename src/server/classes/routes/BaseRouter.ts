import {Router} from 'express';
import {Guard} from '../Guard';
import {Permission} from '../models/Permission';

/**
 *   Базовый роутер.
 *   Всем роутерам необходимо расширять данный клас,
 *   дополняя при этом метод setup.
 *   Сконфигурированный Express.Router будет возвращен данным класом
 *   с помощью Class.instance().get()
 */
export class BaseRouter {
    protected router: Router;
    protected resource: string;

    public static instance() {
        return new this;
    }

    protected constructor() {
        this.router = Router();
    }

    public get() {
        /**
         *  Если ресурс определен, то данный роут является защищенным,
         *  и перед его выполнением следует проверить доступ к нему у вызвавшего его пользователя
         */
        if (this.resource) {
            this.checkPermission();
        }

        return this.setup();
    }

    protected setup (): Router {
        return this.router;
    }

    /**
    *    Проверка на то, разрешен ли для текущего пользователя вызваный роут.
    *    В цепь роутов добавляется звено, в рамках которого проверяется доступ.
    *    Если доступ подтвержден - управление переходит к следующему звену в цепи (next()),
    *    иначе цепь прерывается отправкой ответа, содержащего сообщение об ошибке прав (res.send(...))
    */
    protected checkPermission() {
        this.router.use('*', (req, res, next) => {
            if (!req.cookies.hasOwnProperty('userToken')) {
                res.send({error: 'Permission denied'});
            }
            let data = {token: req.cookies['userToken'], googleToken: req.cookies['googleToken'], resource: this.resource};

            Permission.check(data, req.ip).then((allowed) => {
                if (allowed === true) {
                    next();
                } else {
                    res.send({error: 'Permission denied'});
                }
            }).catch((err) => {
                res.send({error: err});
            });
        });
    }
}
