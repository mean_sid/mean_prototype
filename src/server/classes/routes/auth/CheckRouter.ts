import {Router as ExpressRouter} from 'express';
import {BaseRouter} from '../BaseRouter';
import {Permission} from '../../models/Permission';
import {Guard} from '../../Guard';

export class CheckRouter extends BaseRouter {
    protected setup (): ExpressRouter {
        this.router.get('/isLocalUserCheck', (req, res) => {
            res.json({answer: Guard.isLocalIp(req.ip)});
        });

        this.router.get('', (req, res) => {
            Permission.check(JSON.parse(req.headers['data']), req.ip).then((result) => {
                res.json({allowed: result});
            }).catch((err) => {
                res.json({allowed: false, error: err});
            });
        });

        return super.setup();
    }
}
