import {Router as ExpressRouter} from 'express';
import {BaseRouter} from '../BaseRouter';
import {User} from '../../models/User';
import {Encryptor} from '../../Encryptor';


export class LoginRouter extends BaseRouter {
    protected setup (): ExpressRouter {
        this.router.post('', (req, res) => {
            let requestedUser = JSON.parse(req.body.user);
            User.login(requestedUser).then((user: User) => {
                let token = Encryptor.instance().encryptUser(user);
                res.json(token);
            }).catch((err) => {
                res.json({error: err});
            });
        });

        this.router.post('/google', (req, res) => {
            let user = JSON.parse(req.body.user);

            User.loginByGoogle(user.token).then((user: User) => {
                let token = Encryptor.instance().encryptUser(user);
                res.json(token);
            }).catch((err) => {
                res.json({error: err});
            });

        });

        return super.setup();
    }
}
