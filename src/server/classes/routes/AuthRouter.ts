import {Router as ExpressRouter} from 'express';
import {BaseRouter} from './BaseRouter';
import {LoginRouter} from './auth/LoginRouter';
import {CheckRouter} from './auth/CheckRouter';

/*
    Корневой роутер для реализации авторизации
*/
export class AuthRouter extends BaseRouter {
    protected setup (): ExpressRouter {
        this.router.use('/login', LoginRouter.instance().get());
        this.router.use('/check', CheckRouter.instance().get());

        return super.setup();
    }
}
