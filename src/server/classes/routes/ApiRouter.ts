import {Router as ExpressRouter} from 'express';
import {AclRouter} from './api/AclRouter';
import {SortableRouter} from './api/SortableRouter';
import {BaseRouter} from './BaseRouter';

/**
 *   Корневой роутер для реализации api клиент-серверного взаимодействия, на который попадают запросы /api/*
 */
export class ApiRouter extends BaseRouter {
    protected setup (): ExpressRouter {
        this.router.use('/acl', AclRouter.instance().get());
        this.router.use('/sortable', SortableRouter.instance().get());

        return super.setup();
    }
}
