import * as https from 'https';

declare let Promise: any;

/**
 * Класс, реализующий работу Api Google
 */
export class Google {

    /**
     * Метод выполняет проверку токена. Если токен валидный и действующий,
     * то возвращает Email аккаунта, которому принадлежит токен,
     * иначе происходит Promise.reject с указанием ошибки.
     *
     * @param token string
     */
    public static checkToken(token): Promise<string> {
        return new Promise((resolve, reject) => {
            if (!token || token === 'undefined') {
                reject('Invalid token: ' + token);
            }

            https.request({
                host: 'www.googleapis.com',
                path: '/oauth2/v3/tokeninfo?access_token=' + token,
                method: 'GET'
            }, (res) => {
                res.on('data', (data) => {
                    let response = JSON.parse(data.toString());
                    if (response['email']) {
                        resolve(response['email']);
                    } else {
                        reject('Invalid google token');
                    }
                });
            }).on('error', (e) => {
                reject(e);
            }).end();
        });
    }
}
