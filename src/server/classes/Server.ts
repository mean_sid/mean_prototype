import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as path from 'path';
import * as errorHandler from 'errorhandler';
import * as config from './../environment/config';
import {ApiRouter} from './routes/ApiRouter';
import {AuthRouter} from './routes/AuthRouter';
import {User} from './models/User';
import * as cookieParser from 'cookie-parser';
import {Acl} from './models/Acl';
import {ClientSocket} from './sockets/ClientSocket';
import {TeraSocket} from './sockets/TeraSocket';

/**
 * Корневой класс сервера, который инициализирует приложение
 *
 * @class Server
 */
export class Server {

    public app: express.Application;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return Server Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        // initialization of the app
        this.init();

        // configure application
        this.config();

        // add api
        this.api();
    }

    private init() {
        // create expressjs application
        this.app = express();

        // need for check file exists before use
        User.init();
        Acl.init();

        ClientSocket.init();
        TeraSocket.init();
    }

    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    public api() {
        this.app.use('/api', ApiRouter.instance().get());
        this.app.use('/auth', AuthRouter.instance().get());
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        // add static paths
        if (!config.production) {
            this.app.use(express.static(path.join(__dirname, '../../..'))); // for have node_modules on development mode
        }

        this.app.use(express.static(config.pathToClientDir));

        this.app.use(cookieParser());

        // mount json form parser
        this.app.use(bodyParser.json());

        // mount query string parser
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        // catch 404 and forward to error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        // error handling
        this.app.use(errorHandler());
    }
}
