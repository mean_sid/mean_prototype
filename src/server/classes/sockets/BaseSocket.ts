import * as WS from 'ws';

export class BaseSocket {
    protected ws = WS;
    protected socketConnection;

    protected reconnectTimeout = 30 * 1000; // in milliseconds

    public static init() {
        let instance = new this;
        instance.setup();
        instance.setupErrorHandler();
    }

    constructor() {}

    protected setup(): void {}

    protected setupErrorHandler() {
        this.socketConnection.on('error', (err) => {
            console.log('Socket Error');
            console.log(err);
        });
    }
}
