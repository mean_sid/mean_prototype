import {BaseSocket} from './BaseSocket';
import {teraWsUrl} from '../../environment/config';
import {Tera} from '../models/Tera';

export class TeraSocket extends BaseSocket {

    protected setup(): void {
        this.teraStatusProtocolInit();
    }

    protected teraStatusProtocolInit(tryNumber = 1) {
        let protocol = 'tera-status-protocol';

        /** Открытие Клиент соединения WebSocket */
        this.socketConnection = new this.ws(teraWsUrl, protocol);

        this.socketConnection.on('open', () => {

            /** Обработка сообщений */
            this.socketConnection.on('message', (msg) => {
                /** Хранение сообщений в памяти ноды. Обработка данных. */
                Tera.addItem(protocol, JSON.parse(msg));
            });

            if (tryNumber > 1) {
                console.log(protocol + ' connection successfully restored after ' + tryNumber + ' attempts.');
            }

        });

        /** При ошибке пробуем подключится снова, спустя какое-то времяю */
        this.socketConnection.on('error', () => {
            console.log(
                protocol +
                ' connection throws error, Node will try to open connection again, after ' +
                (this.reconnectTimeout / 1000) + ' sec.'
            );

            setTimeout(() => {
                this.teraStatusProtocolInit(++tryNumber);
            }, this.reconnectTimeout);
        });
    }
}
