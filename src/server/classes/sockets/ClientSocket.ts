import {BaseSocket} from './BaseSocket';
import {Guard} from '../Guard';
import {clientProtocolList, socketServerPort} from '../../environment/config';


/**
 * Класс, реализующий сокет-сервер для взаимодействия клиент-сервера для Web приложения.
 */
export class ClientSocket extends BaseSocket {
    /** Хранение сообщений для тестового модуля Chat */
    private messages: Array<any> = [];

    /** Список поддерживаемых протоколов по данному сокету */
    public handleProtocols(protocols, req) {
        for (let protocol of protocols) {
            if (clientProtocolList.indexOf(protocol) !== -1) {
                req(true, protocol);
            }
        }
    }

    /** Проверка доступа пользователя к ресурсу(протоколу сокета) */
    public verifyClient(info, cb) {
        let regexp = new RegExp('userToken=([a-z0-9]+)');
        let regexpResult = regexp.exec(info.req.headers['cookie']);

        if (
            regexpResult === null || typeof regexpResult[1] !== 'string' ||
            typeof info.req.headers['sec-websocket-protocol'] !== 'string' ||
            info.req.headers['sec-websocket-protocol'].length <= 0
        ) {
            cb(false);
            return;
        }

        let userToken = regexpResult[1];

        Guard.instance().isAllowedResource(
            info.req.headers['sec-websocket-protocol'],
            userToken,
            true
        ).then((allowed) => {
            cb(allowed);
        }).catch((err) => {
            console.log(err);
            cb(false);
        });
    }

    protected setup(): void {
        this.chatInit();

        this.socketConnection = new this.ws.Server({
            server: '//localhost',
            port: socketServerPort,
            handleProtocols: this.handleProtocols,
            verifyClient: this.verifyClient
        });

        this.socketConnection.on('connection', (connection) => {
            connection.send(JSON.stringify(this.messages));
            connection.on('message', (message) => {
                this.messages.push(message);

                this.socketConnection.clients.forEach((client) => {
                    client.send(JSON.stringify(this.messages));
                });
            });
        });
    }

    private chatInit() {

    }
}
