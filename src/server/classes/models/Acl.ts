import {Acl as VirgenAcl} from 'virgen-acl';
import * as DB from 'quick-store';
import * as fs from 'fs';
import * as path from 'path';
import {Role} from './Role';
import {Resource} from './Resource';
import {Permission} from './Permission';
declare let Promise: any;


/*
    Класс, расширяющий virgen-acl для того, что бы привязать к нему
    его собственный функционал для работы с БД - записывать конфигурацию в базу,
    и считывать записанное состояние конфигурации при инициализации.
*/
export class Acl extends VirgenAcl { // Acl - класс стороннего модуля virgen-acl
    private static _instance: Acl;
    private static pathToDataDir = '../../data/acl';
    private static pathToDataFile = Acl.pathToDataDir + '/acl.json';
    private db: DB;

    private roles: Role[];
    private resources: Resource[];
    private permissions: Permission[];

    public static init() {
        this.instance();
    }

    public static instance() {
        if (!this._instance) {
            this._instance = new Acl;
        }
        return this._instance;
    }

    private constructor() {
        super();

        this.checkFile();
        this.db = new DB(path.join(__dirname, Acl.pathToDataFile));
        this.setup().then();
    }


    public getRoles() {
        return this.roles;
    }

    public deleteRole(role: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            delete(this.roles[role]);
            this.save().then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public getPermissions() {
        return this.permissions;
    }

    public setPermissions(permissions: Permission[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.permissions = permissions;
            this.save().then(() => {
                console.log('saved');
                resolve(true);
            });
        });
    }

    /* Сохранить изменения в базу */
    public save(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.put(
                {roles: this.roles, resources: this.resources, permissions: this.permissions},
                () => { resolve(); }
            );
        });
    }

    /** Проверить доступен ли для role данный resource и action */
    public query(role: string, resource: string, action: string|null): Promise<boolean> {
        return new Promise((resolve, reject) => {
            for (let permission of this.permissions) {
                if (
                    permission.role === role &&
                    permission.resource === resource &&
                    permission.allowed
                ) {
                    resolve(permission.allowed);
                }
            }

            resolve(false);
        });
    }

    /**
     * Инициализация экземпляра класса с данными из базы.
     */
    private setup(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.db.get((result) => {
                this.permissions = result.permissions;
                this.roles = result.roles;
                this.resources = result.resources;

                resolve();
            });
        });
    }

    /**
     * Метод проверяет файлы базы данных, создает, если нет. Также создает дефолтного пользователя при создании файлов.
     * Вызывается при инициализации экземпляра класса.
     */
    private checkFile() {
        try {
            fs.accessSync(path.join(__dirname, Acl.pathToDataDir));
        } catch (e) {
            fs.mkdirSync(path.join(__dirname, Acl.pathToDataDir));
        }

        try {
            fs.accessSync(path.join(__dirname, Acl.pathToDataFile));
        } catch (e) {
            fs.writeFileSync(
                path.join(__dirname, Acl.pathToDataFile),
                `{"roles":{"Admin": null},"resources":{},
                "permissions":[{"role": "Admin","resource":"permission","action":null,"allowed":true},
                               {"role": "Admin","resource":"root","action":null,"allowed":true}]}`
            );
            console.log('Default access to permission-control for role: Admin, created');
        }
    }
}
