/**
 * Модель, реализующая обработку и хранение данных, получаемых от TeraWS.
 */
export class Tera {

    /**
     * Объект, хранящий временные данные.
     *
     * @type {}
     */
    private static data: any = {
        'teraStatusItems' : []
    };

    /**
     * Объект, храрящий связи ключей для доступа к данным в объекте data, с используемым протоколом.
     *
     * @type {}
     */
    private static protocolToIndexLinks = {
        'tera-status-protocol' : 'teraStatusItems'
    };

    /**
     * Метод возвращает хранящиеся в пасяти элементы или false, если key не существует.
     *
     * @param key string WS протокол
     * @returns boolean|any
     */
    public static getItems(key: string) {
        if (this.protocolToIndexLinks[key] === 'undefined' || typeof this.data[this.protocolToIndexLinks[key]] === 'undefined') {
            return false;
        }

        return this.data[this.protocolToIndexLinks[key]];
    }

    /**
     * Добавление элемента во временное хранилище.
     * Вызов обработчика, если он задан.
     *
     * @param key string WS протокол
     * @param item
     * @returns {boolean}
     */
    public static addItem(key: string, item: any) {
        if (this.protocolToIndexLinks[key] === 'undefined' || typeof this.data[this.protocolToIndexLinks[key]] === 'undefined') {
            return false;
        }

        /** Добавляем к объекту время его добавления в массив. */
        if (typeof item['unix'] === 'undefined') {
            item['unix'] = Date.now();
        }

        /** Очистка данных из памяти, при достижении лимита. */
        if (this.data[this.protocolToIndexLinks[key]].length > 1000) {
            this.data[this.protocolToIndexLinks[key]].splice(0, 200); // удалить 200 первых записей
        }

        /** Обработка получаемых данных, если существует соответсвтующий метод */
        if (typeof this[this.protocolToIndexLinks[key] + 'Processing'] === 'function') {
            this[this.protocolToIndexLinks[key] + 'Processing'](item);
        }

        this.data[this.protocolToIndexLinks[key]].push(item);
        return true;
    }

    /**
     * Обработка данных.
     */
    private static teraStatusItemsProcessing(item) {

    }
}
