import {Acl} from './Acl';
import {User} from './User';
import {Encryptor} from '../Encryptor';
import {Guard} from '../Guard';
import {Google} from '../Google';
declare let Promise: any;

/**
 * Модель Permission для работы с записями доступов в базе, проверки доступа
 * пользователей к ресурсам.
 */
export class Permission {
    public role: string;
    public resource: string;
    public action?: string | null;
    public allowed?: boolean;

    /**
     * Метод возвращает все записи доступов.
     */
    public static getAll(): Promise<Permission[]> {
        return new Promise((resolve, reject) => {
            resolve(Acl.instance().getPermissions());
        });
    }

    /**
     * Метод удаляет все записи доступов из базы, принадлежащие переданой роли. (используется при удалении роли)
     *
     * @param role string
     */
    public static deleteByRole(role: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let isChanged = false;
            let permissions = Acl.instance().getPermissions();
            for (let key in permissions) {
                if (permissions.hasOwnProperty(key) && permissions[key].role === role) {
                    permissions.splice(+key, 1);
                    isChanged = true;
                }
            }
            if (isChanged) {
                Acl.instance().setPermissions(permissions).then(() => {
                    /* Если были изменения в пермициях, то прогоняем проверку еще раз (рекурсивно),
                     так как могли остаться не удаленные пермиции
                     TODO: Довести метод до ума, чтобы небыло этой рекурсии.*/
                    Permission.deleteByRole(role).then().catch((err) => {
                        reject(err);
                    });
                }).catch();
            } else {
                resolve(true);
            }
        });
    }

    /**
     * Метод принимает объект Permission и переключает доступ.
     * Если доступ был - удаляется, если нет - устанавливается.
     *
     * @param item Permission {role: string, resource: string}
     */
    public static togglePermission(item: Permission): Promise<boolean> {
        return new Promise((resolve, reject) => {

            let isPermissionExists = false;
            let permissions = Acl.instance().getPermissions();
            for (let key in permissions) {
                if (permissions.hasOwnProperty(key)) {
                    if (
                        permissions[key].role === item.role &&
                        permissions[key].resource === item.resource
                    ) {
                        isPermissionExists = true;
                        permissions.splice(+key, 1);
                        break;
                    }
                }
            }
            if (!isPermissionExists) {
                if (Acl.instance().getRoles().hasOwnProperty(item.role)) {
                    Acl.instance().allow(item.role, item.resource);
                }
            }

            Acl.instance().save().then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            });
        })
    }

    /**
     * Основной метод проверки доступа пользователя к ресурсу. Вызывается в Роутерах.
     *
     * @param data
     * @param ip
     */
    public static check(data, ip): Promise<boolean> {
        return new Promise((resolve, reject) => {
            /** Расшифровка токена пользователя. */
            let decryptedUser = Encryptor.instance().decryptUser(data.token);

            /** Получение пользователя из базы по расшифрованому токену. */
            User.get(decryptedUser.name).then((user) => {

                /** Проверка на доступ к ресурсу у Роли пользователя */
                Acl.instance().query(user.getRole(), data.resource, null).then((allowed) => {
                    if (Guard.isLocalIp(ip)) {
                        /** Если подключение локальное, сразу возвращаем результат проверки. */
                        resolve(allowed);
                    } else {
                        /** Если подключение глобальное, дополнительно проверяем GoogleToken */

                        /** Если GoogleToken не передан, выполняем reject */
                        if (!data.googleToken) {
                            reject('Google Authorization need for global connections');
                        }

                        /**
                         * Выполняем проверку GoogleToken. Если токен валидный, дополнительно сравниваем email
                         * зарегистрированного в приложении пользователя и email, возвращаемый сервисом Google.
                         */
                        Google.checkToken(data.googleToken).then((googleUserEmail) => {
                            if (googleUserEmail === user.getEmail()) {
                                resolve(allowed);
                            } else {
                                reject('Google email not match with application current user email');
                            }

                        }).catch((err) => {
                            reject(err);
                        });
                    }
                });
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
