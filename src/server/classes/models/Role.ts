import {Acl} from './Acl';
import {Permission} from './Permission';
declare let Promise: any;


export class Role {
    public name: string;

    public static getAll(): Promise<Role[]> {
        return new Promise((resolve, reject) => {
            resolve(Acl.instance().getRoles())
        });
    }

    public static get(name: string): Promise<Role> {
        return new Promise((resolve, reject) => {
            let roles = Acl.instance().getRoles();
            for (let role in roles) {
                if (roles.hasOwnProperty(role)) {
                    if (role === name) {
                        resolve(new this(role));
                    }
                }
            }
            reject({error: 'Role not found'});
        });
    }

    public static isExists(name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let roles = Acl.instance().getRoles();
            for (let role in roles) {
                if (roles.hasOwnProperty(role) && role === name) {
                    resolve(true);
                    break
                }
            }
            resolve(false);
        });
    }

    constructor(role: string) {
        this.name = role;
    }

    public save(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            Acl.instance().addRole(this.name);
            Acl.instance().save().then(() => {
                resolve(true);
            });
        });
    }

    public deleteRole(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let roles = Acl.instance().getRoles();
            for (let key in roles) {
                if (roles.hasOwnProperty(key)) {
                    if (key === this.name) {
                        Permission.deleteByRole(this.name).then((result) => {
                            if (result) {
                                Acl.instance().deleteRole(this.name).then((result) => {
                                    resolve(result);
                                }).catch((err) => {
                                    reject(err);
                                });
                            } else {
                                resolve(result);
                            }
                        }).catch((err) => {
                            reject(err);
                        });
                    }
                }
            }
        });
    }
}
