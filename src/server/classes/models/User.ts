import * as fs from 'fs';
import * as QuickStore from 'quick-store';
import {Role} from './Role';
import {pathToDataDir} from '../../environment/config';
import {Google} from '../Google';
declare let Promise: any;

/**
 *   ORM модель для работы с пользователями
 */
export class User {
    /**
     * Путь к файлу, хранячему данные о пользователях
     * @type {string}
     */
    private static pathToData = pathToDataDir + '/acl/users.json';

    /**
     * Подключаемый модуль для работы с базой
     */
    private static db: QuickStore;

    /**
     * Данные о дефолтном пользователе, которые добавляются в базу если она пуста.
     * @type {string}
     */
    private static defaultUser = '{"admin":{"name":"admin","role":"Admin","password":"admin","email":"soft12@sidstudio.com.ua"}}';

    /**
     * Имя пользователя.
     */
    private name: string;

    /**
     * Роль пользователя.
     */
    private role: string;

    /**
     * Пароль пользователя.
     */
    private password: string;

    /** Email пользователя */
    private email?: string;

    /**
     * Метод проверяет файлы базы данных, создает, если нет. Также создает дефолтного пользователя при создании файлов.
     * Инициализирует this.db объект, реализующий базу данных.
     * Вызывается единожды при старте node в классе Server.
     */
    static init(): void {
        try {
            fs.accessSync(pathToDataDir + '/acl');
        } catch (e) {
            fs.mkdirSync(pathToDataDir + '/acl');
        }

        try {
            fs.accessSync(User.pathToData);
        } catch (e) {
            fs.writeFileSync(User.pathToData, User.defaultUser);
            console.log('Default user "login: admin; pass: admin; role: Admin" created');
        }

        this.db = new QuickStore(User.pathToData);
    }

    static get(name: string): Promise<User> {
        return new Promise((resolve, reject) => {
            User.db.getItem(name, (user: User|undefined) => {
                if (user) {
                    resolve(new User(user.name, user.role, user.password, user.email));
                } else {
                    reject('User not found');
                }
            });
        });
    }

    static getAll(forRout = false): Promise<User[]> { // forApi - set true to return only data, without passwords
        return new Promise((resolve, reject) => {
            User.db.get((users) => {
                let result = [];

                for (let key in users) {
                    if (users.hasOwnProperty(key)) {
                        if (forRout) {
                            result.push({name: users[key].name, role: users[key].role, email: users[key].email});
                        } else {
                            result.push(new User(users[key].name, users[key].role, users[key].password, users[key].email));
                        }
                    }
                }
                resolve(result);
            });
        });
    }

    public static getDb(): QuickStore {
        return this.db;
    }

    public static login(enteredUser): Promise<User> {
        return new Promise((resolve, reject) => {
            User.get(enteredUser.name).then((user) => {
                user.checkPassword(enteredUser.password).then((isCorrect) => {
                    if (isCorrect) {
                        resolve(user);
                    } else {
                        reject('Incorrect password');
                    }
                }).catch((err) => {
                    reject(err);
                });
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public static loginByGoogle(token): Promise<User> {
        return new Promise((resolve, reject) => {
            Google.checkToken(token).then((email) => {
                this.getByEmail(email).then((user) => {
                    resolve(user);
                });
            }).catch((err) => {
                reject(err);
            });
        });
    }

    private static getByEmail(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.getAll().then((users) => {
                for (let user of users) {
                    if (user.email === email) {
                        resolve(user);
                    }
                }
            });
        });
    }

    constructor (login: string, role: string, pass: string, email?: string) {
        if (login && role && pass) {
            this.name = login;
            this.role = role;
            this.password = pass;
            if (email) {
                this.email = email;
            }
        } else {
            throw new Error('Incorrect arguments for create user');
        }
    }

    public getName(): string {
        if (this.name) {
            return this.name;
        } else {
            return '';
        }
    }

    public getRole(): string {
        if (this.role) {
            return this.role;
        } else {
            return '';
        }
    }

    public getPassword(): string {
        if (this.password) {
            return this.password;
        } else {
            return '';
        }
    }

    public getEmail(): string {
        if (this.email) {
            return this.email;
        } else {
            return '';
        }
    }

    private insert() {
        return new Promise((resolve, reject) => {
            User.db.getItem(this.name, (user) => {
               if (user) {
                   reject('User is exists!');
               } else {
                   User.db.setItem(this.name, this.toJson(), () => {
                       resolve(true);
                   });
               }
            });
        });
    }

    public save() {
        return new Promise((resolve, reject) => {
            User.db.setItem(this.name, this.toJson(), () => {
                resolve(true);
            });
        });
    }

    public deleteUser() {
        return new Promise((resolve, reject) => {
            User.db.removeItem(this.name, (users) => {
                resolve(users);
            });
        });
    }

    public setRole(role: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            Role.isExists(role).then((roleExists) => {
                if (!roleExists) {
                    resolve(false);
                } else {
                    this.role = role;
                    this.save().then(() => {
                        resolve(true);
                    });
                }
            });
        });
    }

    public checkPassword(pass: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(this.password === pass);
        });
    }

    private toJson() {
        let json = {
            'name': this.name,
            'role': this.role,
            'password': this.password
        };
        if (this.email) {
            json['email'] = this.email;
        }

        return json;
    }
}

