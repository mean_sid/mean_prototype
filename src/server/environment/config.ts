import * as path from 'path';
// paths to client directories
const pathToClientDevDir = path.join(__dirname, '../../../dev-dist/client');
const pathToClientProdDir = path.join(__dirname, '../../client');



/** Конфигурация Web приложения. Основные настройки. */

/**
 *   production = true - использовать в качестве клиента dist/client
 *   production = false - использовать в качестве клиента dev-dist/client
 */
export const production = true;

/** Порт, по которому запускается Node */
export const port = 9001;

/** url для сокет-соединения с TeraWS */
export const teraWsUrl = 'ws://192.168.50.100:7681';

/**
 * Настройки доступа для сокет-соединений между клиентом и сервером Web приложения.
 * Свойства объекта - роли пользователей, значения - массивы разрешенных протоколов.
 */
export const clientProtocolAccess = {
    admin: [
        'chat',
        'connection'
    ]
    // TODO: добавить дефолтный массив протоколов,  разрешенных для всех пользователей.
};

/**
 * Массив всех активных протоколов для сокет-соединений между клиентом и сервером Web приложения.
 * Можно отключить любой протокол, он перестанет быть доступен для клиентов.
 */
export const clientProtocolList = [
    'chat',
    'connection'  // Основной протокол-соединение, определяющее связь клиентов с сервером.
];
/** -------------------------- */




/** Системные настройки */

/** Путь к корню клиентской директории */
export const pathToClientDir = production ? pathToClientProdDir : pathToClientDevDir;

/** Путь к директории хранения данных */
export const pathToDataDir = path.join(__dirname, '../data');

/** Порт, на котором запущен сокет сервер, для связи клиент-сервер Web приложения. */
// TODO: реализовать динамическую отдачу клиенту
export const socketServerPort = 9082;

/** -------------------------- */

