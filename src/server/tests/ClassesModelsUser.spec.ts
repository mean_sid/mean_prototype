import {User} from '../classes/models/User';
import {Role} from '../classes/models/Role';
declare let Promise: any;


describe('User Model Tests', () => {
    let user: User;

    beforeEach(() => {
        User.init();
        user = new User('testName', 'testRole', 'testPass');
    });

    it('user.get(Name|Role|Password) should returns test values', (done) => {
        expect(user.getName()).toEqual('testName');
        expect(user.getRole()).toEqual('testRole');
        expect(user.getPassword()).toEqual('testPass');

        done();
    });

    it('user.setRole should execute user.save, and changed user.role value', (done) => {
        let spySave = spyOn(user, 'save').and.returnValue(Promise.resolve(true));
        let spyIsExists = spyOn(Role, 'isExists').and.returnValue(Promise.resolve(true));

        user.setRole('newTestRole').then(() => {
            expect(spySave.calls.count()).toEqual(1);
            expect(spyIsExists.calls.count()).toEqual(1);
            expect(user.getRole()).toEqual('newTestRole');

            done();
        });
    });

    it('user.checkPassword works', (done) => {
        user.checkPassword('testPass').then((bool) => {
            expect(bool).toEqual(true);

            user.checkPassword('wrongPass').then((bool) => {
                expect(bool).toEqual(false);
                done();
            });
        });
    });
    
    it('User.get() test', (done) => {
        let spyGetItem = spyOn(User.getDb(), 'getItem').and.callFake((name, callback) => {
            callback({'name': 'mockUserName', 'role': 'mockUserRole', 'password': 'mockUserPass'});
        });

        User.get('mockUser').then((user) => {
            expect(spyGetItem.calls.count()).toEqual(1);
            expect(user.getName()).toEqual('mockUserName');

            done();
        });
    });

    it('User.getAll() test', (done) => {
        let spyGet = spyOn(User.getDb(), 'get').and.callFake((callback) => {
            callback([
                {'name': 'mockUser1Name', 'role': 'mockUser1Role', 'password': 'mockUser1Pass'},
                {'name': 'mockUser2Name', 'role': 'mockUser2Role', 'password': 'mockUser2Pass'}
            ]);
        });

        User.getAll().then((users) => {

            expect(spyGet.calls.count()).toEqual(1);
            expect(users.length).toEqual(2);
            expect(users[0].getName()).toEqual('mockUser1Name');

            done();
        });
    });
});
