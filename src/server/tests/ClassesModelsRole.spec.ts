import {Role} from '../classes/models/Role';
import {Acl} from '../classes/models/Acl';
declare let Promise: any;


describe('Role Model Tests', () => {
    let role: Role;

    beforeEach(() => {
        role = new Role('testRoleName');
    });

    it('role created', (done) => {
        expect(role.name).toEqual('testRoleName');

        done();
    });

    it('Role.get() test', (done) => {
        let spyGetRoles = spyOn(Acl.instance(), 'getRoles').and.returnValue({
            'testRole1Name' : null,
            'testRole2Name' : null
        });

        Role.get('testRole2Name').then((role) => {
            expect(spyGetRoles.calls.count()).toEqual(1);
            expect(role.name).toEqual('testRole2Name');

            done();
        });
    });

    it('Role.getAll() test', (done) => {
        let spyGetRoles = spyOn(Acl.instance(), 'getRoles').and.returnValue({
            'testRole1Name' : null,
            'testRole2Name' : null
        });

        Role.getAll().then((roles) => {
            expect(spyGetRoles.calls.count()).toEqual(1);
            expect(roles['testRole1Name']).toBeNull();

            done();
        });
    });

    it('Role.isExists() test', (done) => {
        let spyGetRoles = spyOn(Acl.instance(), 'getRoles').and.returnValue({
            'testRole1Name' : null,
            'testRole2Name' : null
        });

        Role.isExists('testRole2Name').then((bool) => {
            expect(spyGetRoles.calls.count()).toEqual(1);
            expect(bool).toEqual(true);

            done();
        });
    });

    it('role.save() tests', (done) => {
        let spySave = spyOn(Acl.instance(), 'save').and.returnValue(Promise.resolve());

        role.save().then(() => {
            Role.isExists('testRoleName').then((bool) => {
                expect(spySave.calls.count()).toEqual(1);
                expect(bool).toBeTruthy();
            });
        });
    });
});
