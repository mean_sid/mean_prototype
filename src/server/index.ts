#!/usr/bin/env node
'use strict';

import * as config from './environment/config';
import * as path from 'path';

// module dependencies
const server = require('./classes/Server');
const debug = require('debug')('express:server');
const http = require('http');

// create http server
const httpPort = config.port;
const app = server.Server.bootstrap().app;

/*
 Последний роут. Если ни один роут не подошел, и не произошла ошибка,
 то отдаем в ответ на запрос index.html.
 Это необходимо для прямых непосредственных переходов на странице в клиенте
 через адресную строку. Необходимо доработать, что бы не возвращять html в ответ на
 ошибочные запросы на css и js.
 */
app.get('*', function(req: any, res: any, next: any){
    if (!req.xhr) {
        res.sendFile('index.html', {root: config.pathToClientDir});
    }
});

app.set('port', httpPort);
const httpServer = http.createServer(app);

// listen on provided ports
httpServer.listen(httpPort);
console.log('Listening start on port :' + httpPort);



